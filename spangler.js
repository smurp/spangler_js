/*
  spangler: fractal string art toy
  author: Shawn Murphy <smurp@smurp.com>
  date: 2012.06.26

    http://docs.phonegap.com/en/1.8.1/cordova_accelerometer_accelerometer.md.html#Accelerometer

  */
var show_spec_here;
var show_orientation_here;
var show_dimensions_here;
function hsv2rgb(hue, sat, val) {
  // from: 
  //    http://www.actionscript.org/forums/archive/index.php3/t-15155.html
  // see also:
  //    http://www.webreference.com/programming/javascript/mk/column3/creating/cp_mini_gradient_details.png
    var red, grn, blu, i, f, p, q, t;
    hue%=360; // probably not needed
    if(val==0) {return("rgb(0,0,0)");}
    sat/=100;
    val/=100;
    hue/=60;
    i = Math.floor(hue);
    f = hue-i;
    p = val*(1-sat);
    q = val*(1-(sat*f));
    t = val*(1-(sat*(1-f)));
    if (i==0) {red=val; grn=t; blu=p;}
    else if (i==1) {red=q; grn=val; blu=p;}
    else if (i==2) {red=p; grn=val; blu=t;}
    else if (i==3) {red=p; grn=q; blu=val;}
    else if (i==4) {red=t; grn=p; blu=val;}
    else if (i==5) {red=val; grn=p; blu=q;}
    red = Math.floor(red*255);
    grn = Math.floor(grn*255);
    blu = Math.floor(blu*255);
    var r_g_b = [red,grn,blu];
  //document.spangle_controls.status.value = r_g_b.valueOf();
    return "rgb(" + r_g_b.valueOf() + ")";
}


if (typeof Object.create !== 'function') {
  // from "Javascript: The Good Parts" by Douglas Crockford, page 22
    Object.create = function (o) {
        var F = function () {};
        F.prototype = o;
        F.__proto__ = o;
        return new F();
    }
};


var two_pi = 3.1415926 * 2;
var Spangler = {
    get_spangle_spec_pretty: function(){
	var retval = "";
	var num;
	lines = 1;
	for (var i = 0; i< this.spangle_spec.length; i++){
	        if (i > 0) lines = lines * this.spangle_spec[i];
	        num = this.spangle_spec[i];
	        if (this.is_the_color_spang(i)){
		    num = "'" + num + "'";
		        }
            if (this.current_spang_idx == i){
		retval += "(" + num + ")";
            } else {
		retval += " " + num + " ";
            }
	    }
	retval += " ==>  " + lines + " lines";
	return retval;
    },
    get_spangle_spec_hash: function(){
	var retval = "";
	var num;
	lines = 1;
	for (var i = 0; i< this.spangle_spec.length; i++){
	        if (i > 0) lines = lines * this.spangle_spec[i];
	        num = this.spangle_spec[i];
	        if (this.is_the_color_spang(i)){
		    num = "'" + num + "'";
		        }
            if (this.current_spang_idx == i){
		retval += "(" + num + "),";
            } else {
		retval += num + ",";
            }
	    }
	
	return retval.substr(0,retval.length-1);
    },
    draw_circle : function(cx, cy, radius, clr) {
        var ctx = this.ctx;
        ctx.fillStyle = clr;
        ctx.beginPath();
        ctx.arc(cx,cy,radius, 0, Math.PI*2, true);
        ctx.closePath();
        ctx.fill();
    },
    current_spang_idx : 1,
    //increment : function(){
    go_up : function(){
        this.spangle_spec[this.current_spang_idx]++;
    },
    //decrement : function(){
    go_down : function(){
        if (this.spangle_spec[this.current_spang_idx] > 1){
            this.spangle_spec[this.current_spang_idx]--;
        }
    },
    go_right : function(){
        this.current_spang_idx++;
        if (this.current_spang_idx >= this.spangle_spec.length) {
            this.spangle_spec[this.current_spang_idx] = 1;
        };
    },
    go_left : function(){
	if (! this.delete_dangling_one()){
            this.current_spang_idx--;
            if (this.current_spang_idx < 0){
		this.current_spang_idx = 0;
            };
	    }
    },
    at_last_spang: function(){
	// is the current spang is the rightmost?
	return (this.current_spang_idx === this.spangle_spec.length - 1);
    },
    delete_dangling_one: function(){
	if (this.at_last_spang() && this.spangle_spec[this.current_spang_idx] < 2){
	        this.del_spang();
	        //this.current_spang_idx--;
	        return true;
	    }
	return false;
    },
    del_spang : function(){
        if (this.current_spang_idx < 2) return;
        this.spangle_spec.splice(this.current_spang_idx,1);
        if (this.current_spang_idx >= this.spangle_spec.length){
            this.current_spang_idx = this.spangle_spec.length -1;
	        if (this.color_spang > this.current_spang_idx){
		    this.color_spang = this.current_spang_idx;
		        }
        }
        //this.draw();
    },
    draw_star : function (cx, cy, radius, start_angle, nstep, npoints, in_clr){
        //var rotate_colors = npoints < 0;
	var npoints = abs(npoints);
	//console.log("draw_star",clr);
        var angle_inc = two_pi / npoints;
        var start_x, start_y, end_x, end_y;
	var clr = in_clr;
        for ( j = 0; j < npoints; j++) { 
            start_x = cx + radius * Math.cos(j * angle_inc + start_angle);
            start_y = cy + radius * Math.sin(j * angle_inc + start_angle);
            end_x = cx + radius * Math.cos((nstep + j) * angle_inc + start_angle);
            end_y = cy + radius * Math.sin((nstep + j) * angle_inc + start_angle);
	        if (in_clr === undefined){
		    clr = this.calculate_color(j,npoints,clr);
		        }
	        //console.log("   ",clr);
            this.ctx.strokeStyle = clr;
            this.ctx.beginPath();
            this.ctx.moveTo(start_x,start_y);
            this.ctx.lineTo(end_x,end_y);
            this.ctx.closePath();
            this.ctx.stroke();
           //line(start_x, start_y, end_x, end_y,128);
        }
    },
    init : function(spangle_spec){
        this.spangle_spec = spangle_spec;
	//this.color_spang = 1;
        return this;
    },
    calculate_color : function(which, of_many, default_color, rotate_colors){
	//console.log("     ",which,of_many);
        return hsv2rgb(which/of_many * 360, 100, 100);
        if (rotate_colors){
            return hsv2rgb(which * 1.0 * 256/of_many, 100, 100);
        } else {
            return default_color;
        }
    },
    draw : function(){
        panel.show_spec();
        this.draw_circle(this.cx,this.cy,this.radius,'black');
        this.draw_spangler(this.cx,           this.cy, 
                           this.radius,       this.start_angle, 
                           this.spangle_spec, this.default_color);
        return this;
    },
    color_by_current_number: function(){
	if (this.current_spang_idx){
	        // we can only color by numbers other than the first
            this.color_spang = this.current_spang_idx;
	    }
    },
    rotate_the_color_spang: function(){
	if (this.color_spang < this.spangle_spec.length - 1){
	        this.color_spang++;
	    } else {
		    this.color_spang = 1;
		}
    },
    is_the_color_spang: function(this_idx){
	//console.log(this.color_spang,this_idx);
	return this.color_spang == this_idx;
    },
    draw_spangler : function (cx,           cy,
                              radius,       start_angle,
                              spangle_spec, default_color){
        var angle_inc, child_radius, child_x, child_y, child_angle;
        var child_spec = [].concat(spangle_spec);
	var child_color = default_color;
	var color_the_kids = this.is_the_color_spang(spangle_spec.length -1);
        if (spangle_spec.length > 2) {
            var last_element = child_spec.pop();
            var num_children = abs(last_element);
            angle_inc = two_pi / num_children;
            child_radius = radius / 2;
            this.child_thingy = spangle_spec;
            for (var j = 0; j < num_children; j++) {
                child_x = cx + child_radius * Math.cos(j * angle_inc + start_angle);
                child_y = cy + child_radius * Math.sin(j * angle_inc + start_angle);
                child_angle = start_angle + j * angle_inc;
		if (color_the_kids){
                    child_color = this.calculate_color(j, num_children, default_color);
		    }
                this.draw_spangler(child_x,      child_y,
                                   child_radius, child_angle,
                                   child_spec,   child_color);
            }
        } else {
	        if (this.is_the_color_spang(1)) default_color = undefined;
            this.draw_star(cx, cy, radius, start_angle, 
                           spangle_spec[0], spangle_spec[1], default_color);
        }
    },
    default_spec : [1,5,-6],
};
var abs = function(a)  {if (a < 0) return a * -1;  return a;};
var min = function(a,b){if (a < b) {return a} else {return b}};
var SpanglePanel = {
    set_spangle_spec_from_form : function(){
	var initial_spec = location.hash;
	if (initial_spec[0] == '#'){
	        initial_spec = initial_spec.substr(1,initial_spec.length);
	    }
	if (initial_spec.length == 0) initial_spec = "1,'5',2,9,3";
	//document.spangle_controls.spanglespec.value
        try {
	        //alert(initial_spec);
            this.all[0].spangle_spec = eval("[" + initial_spec + "]");
        } catch (e) {
	        //alert(e);
            this.all[0].spangle_spec = [1,5,2,9];
        }
        return this;
    },
    set_form_from_spangle_spec : function(){
        try {
            document.spangle_controls.spanglespec.value = this.all[0].spangle_spec.valueOf();
            document.spangle_controls.number_of_lines.value = eval(this.all[0].spangle_spec.join(' * '));
        } catch (e) {
            
        }
        return this;
    },
    rightDown : false,
    leftDown  : false,
    upDown    : false,
    downDown  : false,
    delDown   : false,
    decelDown : false, //s
    accelDown : false, //w
    
    onKeyUp: function(evt){
        if (evt.keyCode == 39)      {this.rightDown = false;}
        else if (evt.keyCode == 37) {this.leftDown  = false;}
        else if (evt.keyCode == 40) {this.downDown  = false;}
        else if (evt.keyCode == 38) {this.upDown    = false;}
        else if (evt.keyCode == 46) {this.delDown   = false;}
        else if (evt.keyCode == 83) {this.decelDown = false;}
        else if (evt.keyCode == 87) {this.accelDown = false;}
    },
    onKeyDown: function(evt){
        if (evt.keyCode == 39)      {this.rightDown = true;}
        else if (evt.keyCode == 37) {this.leftDown  = true;}
        else if (evt.keyCode == 40) {this.downDown  = true;}
        else if (evt.keyCode == 38) {this.upDown    = true;}
        else if (evt.keyCode == 46) {this.delDown   = true;}
        else if (evt.keyCode == 83) {this.decelDown = true;}
        else if (evt.keyCode == 87) {this.accelDown = true;}
    },
    draw : function() {
        this.set_spangle_spec_from_form();

        if (this.rightDown) { this.all[0].go_right(); }
        else if (this.leftDown) { this.all[0].go_left(); }
        if (this.upDown) { this.all[0].increment(); }
        else if (this.downDown) { this.all[0].decrement(); }
        if (this.delDown) { this.all[0].del_spang(); }

        this.set_form_from_spangle_spec();
        for (var idx = 0 ; idx < this.all.length; idx++ ) {
            this.set_size(this.all[idx],idx).draw();
        };
    },
    set_size : function(spang,idx) {
        var min_dim = min(this.canvas.width,
                          this.canvas.height);
      //min_dim = this.canvas.width; // FIXME, this should not be needed

        var radius = min_dim/2;
        spang.ctx = this.ctx;
        spang.start_angle = two_pi/-4;
        if (idx == 0) {
            spang.radius = radius;
            spang.cx = radius;
            spang.cy = radius;
        } else if (0 < idx && idx < 5) {
            var divisor = 6;
            var mini_radius = radius/divisor;
            spang.radius = mini_radius;
            if (idx % 2) { // idx is even
                spang.cx = mini_radius;
            } else {
                spang.cx = (divisor * 2 - 1) * mini_radius;
            };
            if (idx < 3) { // 
                spang.cy = mini_radius;
            } else {
                spang.cy = (divisor * 2 - 1) * mini_radius;
            };
        };

        spang.draw_circle(spang.cx,spang.cy,spang.radius,"#000000");
        return spang;
    },
    all : [],
    init_mouse : function() {
    //this.canvasMinX = this.canvas.offset().left;
    //this.canvasMaxX = this.canvasMinx + this.canvas.width;
    },
    resize_canvas_to_fit_window : function(){
        var padding = {'x': 30, 'y': 130};
        padding = {'x': 20, 'y': 20};
	var min_dim = Math.min(window.innerWidth - padding.x, 
			              window.innerHeight - padding.y);
        this.canvas.width = min_dim;
        this.canvas.height = min_dim;
	if (show_dimensions_here){
	        show_dimensions_here.innerHTML = window.innerWidth +  "x" + window.innerHeight +
		"  " + this.canvas.width +"x"+this.canvas.height;
	    }
	
    },
    /* all the different control systems will call the go_ functions 
       e.g. turn_left will call go_left
            tip_forward will call go_down (think plane stick)
	        */
    delete_current_number: function(){
        panel.all[0].splice(panel.current_number_index,1);
    },
    submit_spangle: function(){
        var new_spangle = panel.all[0];
        panel.all.unshift(new_spangle);
    },
    handle_deviceorientation: function(evt){
	var beta_neutral = 45;
	var gamma_neutral = 0;
	var threshold = 15;
	var beta_zeroed = Math.round(evt.beta - beta_neutral);
	var gamma_zeroed = Math.round(evt.gamma - gamma_neutral);
	var time_threshold = 500; //milliseconds
	panel.going_right = false;
	panel.going_left = false;
	panel.going_up = false;
	panel.going_down = false;
	left_right = "    ";
	if (gamma_zeroed > threshold){
	        panel.going_right = true;
	        left_right = 'rite';
	    }
	if (gamma_zeroed + threshold < 0){
	        panel.going_left = true;
	        left_right  = 'left';
	    }
	up_down = "  ";
	if (beta_zeroed > threshold){
	        panel.going_down = true;
	        up_down = "dn";
	    }
	if (beta_zeroed + threshold < 0){
	        panel.going_up = true;
	        up_down = "up";
	    }
	if (show_orientation_here){
	        show_orientation_here.innerHTML = ""+ [
		    "",Math.round(evt.alpha), Math.round(evt.beta), Math.round(evt.gamma)];
	        show_orientation_here.innerHTML = up_down+"  "+left_right;
	    }
	var now = (new Date()).getTime();
	if (! panel.last_orient_time){
	        panel.last_orient_time = now;
	        return;
	    }
	if ((now - panel.last_orient_time) > time_threshold){
	    panel.last_orient_time = now;    
	    } else {
		    return;
		}
	panel.perform_operations();
    },
    handle_keyboard: function(evt){
        //console.log(evt.keyIdentifier, evt.type);
	var retval = true;
        var new_state;
        if (evt.type == "keyup"){
            new_state = false;
        }
        if (evt.type == "keydown"){
            new_state = true;
        }
        if (evt.keyIdentifier == "Up"){
            panel.going_up = new_state;
	        retval = false;
        }           
        if (evt.keyIdentifier == "Down"){
            panel.going_down = new_state;
	        retval = false;
        }
        if (evt.keyIdentifier == "Left"){
            panel.going_left = new_state;
	        retval = false;
        }
        if (evt.keyIdentifier == "Right"){
            panel.going_right = new_state;
	        retval = false;
        }
        if (evt.keyIdentifier == "Enter"){
	        console.log(panel.coloring_by_current_number,panel.rotating_the_coloring);
	        if (panel.coloring_by_current_number || panel.rotating_the_coloring){
		    panel.rotating_the_coloring = true;
		    panel.coloring_by_current_number = false;
		        } else {
			    panel.coloring_by_current_number = new_state;
			        }
        } else {
	        panel.rotating_the_coloring = false;
	    panel.coloring_by_current_number = false;    
	    }
        if (evt.keyIdentifier == "U+0008"){
	        retval = false;
            panel.deleting_current_number = new_state;
        }

	var time_threshold = 90;
	var now = (new Date()).getTime();
	if (! panel.last_key_time){
	        panel.last_key_time = now;
	        return retval;
	    }
	if ((now - panel.last_key_time) > time_threshold){
	    panel.last_key_time = now;    
	    } else {
		    return retval;
		}

        panel.perform_operations();
        return retval;
    },
    perform_operations: function(){
        //console.log(panel);
        var the_spangler = panel.all[0];
        if (panel.going_up) the_spangler.go_up();
        if (panel.going_down) the_spangler.go_down();
        if (panel.going_left) the_spangler.go_left();
        if (panel.going_right) the_spangler.go_right();
        if (panel.deleting_current_number) the_spangler.del_spang();
	
        if (panel.coloring_by_current_number){
	        //panel.coloring_by_current_number = false;
	        the_spangler.color_by_current_number();
	    }
	if (panel.rotating_the_coloring){
	        the_spangler.rotate_the_color_spang();
	    }
        the_spangler.draw();
        //console.log(the_spangler.spangle_spec,the_spangler.current_spang_idx);
    },
    init : function() {
        // Make sure we don't execute when canvas isn't supported
        this.canvas = document.getElementById('spangler');
        this.current_number_index = 1;
        if (this.canvas.getContext){
            this.resize_canvas_to_fit_window();
            this.init_mouse();
            this.ctx = this.canvas.getContext('2d');
            this.all = [Object.create(Spangler).init([1,3]),
                        //Object.create(Spangler).init([1,3,5,2,9]),
                        //Object.create(Spangler).init([1,3,6,6]),
                        //Object.create(Spangler).init([1,3,3,3]),
                        //Object.create(Spangler).init([1,4,5,5,2,12,3]),
                        //Object.create(Spangler).init([2,7,-4])
			];
	        /*
		        1,4,2,'18',1,(5),2,9
			     */
            this.draw();  
              //setInterval(panel.draw,1000);
              // THESE NEXT TWO LINES DO NOT YET WORK
            //document.spangle_controls.status.value = type(this.canvas.onkeydown).toValue();
              //this.canvas.onkeydown = this.onKeyDown;
              //this.canvas.onkeyup = this.onKeyUp;
            document.onkeydown = this.handle_keyboard;
            document.onkeyup   = this.handle_keyboard;
        } else {
            alert('Safari or Firefox > 1.5 needed.');
        }
	//Enable device orientation sensitivity:
        //window.ondeviceorientation = this.handle_deviceorientation;
    },
    show_spec : function(){
      if (history.pushState){
         var the_hash = this.all[0].get_spangle_spec_hash();
	   var new_state = location.href.replace(location.hash,"") +"#"+ the_hash;
         //alert(the_spec);
	   //console.log('location',location);
	   var new_title = "Spangle:"+the_hash;
         history.pushState({},new_title,new_state);
	   document.title = new_title;
      }
      if (show_spec_here){
	    //show_spec_here.innerHTML = new_state;
	    show_spec_here.innerHTML = this.all[0].get_spangle_spec_pretty();
      }
    }
};
