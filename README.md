spangler_js
===========

Spangler is a **fractal string art toy** for making little things like:

![spangler movie](/spangler_movie.gif "a little spangler movie")

The rotation is not yet implemented in this Javascript version (it is from an older Postscript version).

[try it here](http://www.smurp.com/Spangler/Javascript)

usage
-----
* `up arrow` increase value
* `down arrow` decrease value
* `right` move to next term
* `left` move to previous term  
* `enter` mark the term as the one to color, or cycle colors
* `del` delete the term

browsers:
---------
* Safari: works well
* Chrome: works well
* Firefox: an issue has been reported but not fixed
* IE: unknown

